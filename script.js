const images = Array.from(document.querySelectorAll('.image-to-show'));
const stopButton = document.getElementById('stopButton');
const resumeButton = document.getElementById('resumeButton');
let currentIndex = 0;
let intervalId;
let isResumeButtonActive = true;

function showNextImage() {
  images.forEach(image => {
    image.style.display = 'none';
  });

  images[currentIndex].style.display = 'block';
  currentIndex = (currentIndex + 1) % images.length;
}

function startSlideshow() {
  showNextImage();
  intervalId = setInterval(showNextImage, 3000);
  stopButton.addEventListener('click', stopSlideshow);
  resumeButton.removeEventListener('click', resumeSlideshow);
  isResumeButtonActive = false;
}

function stopSlideshow() {
  clearInterval(intervalId);
  stopButton.removeEventListener('click', stopSlideshow);
  if (!isResumeButtonActive) {
    resumeButton.addEventListener('click', resumeSlideshow);
    isResumeButtonActive = true;
  }
}

function resumeSlideshow() {
  startSlideshow();
}

stopButton.addEventListener('click', stopSlideshow);
resumeButton.addEventListener('click', resumeSlideshow);

startSlideshow();